﻿/* codigo para generar  ejemplo3sql */

-- crea base de datos
DROP DATABASE IF EXISTS ejemplo3sql;
CREATE DATABASE ejemplo3sql;
USE ejemplo3sql;

-- creando tablas

-- Profesores
CREATE TABLE profesor(
  -- campos
  dni char(9),
  nombre varchar(25),
  direccion varchar(50),
  telefono varchar(15),

  -- claves
  PRIMARY KEY(dni)
);

-- Modulos
CREATE TABLE modulo(
  -- campos
  cod int AUTO_INCREMENT,
  nombre varchar(50),

  -- claves
  PRIMARY KEY(cod)
);

-- Alumnos
CREATE TABLE alumno(
  -- campos
  nExpediente int AUTO_INCREMENT,
  nombre varchar(25),
  apellidos varchar(30),
  fNac date,

  -- claves
  PRIMARY KEY(nExpediente)
);

-- imparten
CREATE TABLE imparte(
  -- campos
  dniProfesor char(9),
  codModulo int,

  -- claves
  PRIMARY KEY(dniProfesor,codModulo),

  UNIQUE KEY(codModulo),

  -- claves ajenas
  CONSTRAINT FKimparteprofesor FOREIGN KEY(dniProfesor)
  REFERENCES profesor(dni),
  
  CONSTRAINT FKimpartemodulo FOREIGN KEY(codModulo)
  REFERENCES modulo(cod)
);

-- se matriculan
CREATE TABLE matricula(
  -- campos
  codModulo int,
  nExpAlumno int,

  -- claves
  PRIMARY KEY(codModulo,nExpAlumno),

  -- claves ajenas
 CONSTRAINT FKmatriculamodulo FOREIGN KEY(codModulo)
 REFERENCES modulo(cod),

 CONSTRAINT FKmatriculaalumno FOREIGN KEY(nExpAlumno)
 REFERENCES alumno(nExpediente)
);

-- Delegado
CREATE TABLE delegado(
  -- campos
  expDelegado int,
  expEstudiante int,

  -- claves
  PRIMARY KEY(expDelegado,expEstudiante),

  UNIQUE KEY(expEstudiante),

  -- claves ajenas
  CONSTRAINT FKdelegadoalumno FOREIGN KEY(expDelegado)
  REFERENCES alumno(nExpediente),

  CONSTRAINT FKestudiantealumno FOREIGN KEY(expEstudiante)
  REFERENCES alumno(nExpediente)
);